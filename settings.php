<?php

/**
 * @file
 * Private is used so it needs a higher weight than the private path setting.
 *
 * Weight is in the filename.
 */

$databases['default']['default'] = [
  'database' => getenv('DRUPAL_SQLITE_DATABASE_PATH') ?: $settings['file_private_path'] . '/db.sqlite',
  'prefix' => getenv('DRUPAL_SQLITE_DATABASE_PREFIX') ?: '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\sqlite',
  'driver' => 'sqlite',
];
